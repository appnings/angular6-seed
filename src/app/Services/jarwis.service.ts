import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app.component';

@Injectable()

export class JarwisService {
  constructor(private http: HttpClient) { }

  signup(data) {
    return this.http.post(AppSettings.API_SIGNUP, data)
  }

  login(data) {
    return this.http.post(AppSettings.API_LOGIN, data)
  }

  sendPasswordResetLink(data) {
    // return this.http.post(`${this.baseUrl}/sendPasswordResetLink`, data)
  }
  
  changePassword(data) {
    // return this.http.post(`${this.baseUrl}/resetPassword`, data)
  }

}