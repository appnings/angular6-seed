import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
}

const API_ENDPOINT = 'http://blog.test/api/';

export class AppSettings {
  //Login api
  public static API_LOGIN = API_ENDPOINT + 'login';

  //signup api
  public static API_SIGNUP = API_ENDPOINT + 'signup';

  //user profile api
  public static API_USER = API_ENDPOINT + 'profile';

  //logout api
  public static API_LOGOUT = API_ENDPOINT + 'logout';

}