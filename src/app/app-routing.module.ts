import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BeforeLoginService } from './Services/before-login.service';
import { AfterLoginService } from './Services/after-login.service';

const appRoutes: Routes = [
  {
    'path': 'login',
    'component': LoginComponent,
    'canActivate': [BeforeLoginService]
  },
  {
    'path': 'signup',
    'component': SignupComponent,
    'canActivate': [BeforeLoginService]
  },
  {
    'path': 'profile',
    'component': ProfileComponent,
    'canActivate': [AfterLoginService]
  },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
